Database created with phpmyadmin

It is a database that includes cosmetic recipes.
The name of the database "cosmetic_recipe"

the script to create this database is in the sql folder, it is composed of three files:
    "cosmetic_recipe.sql" file that matches the database.
    "cosmetic_recipe_données.sql" which corresponds to the data.
    "cosmetic_recipe_structure.sql" which corresponds to the structure.

A "procedure" folder that contains two files one for backup and the other to restore the database.

It includes 4 tables "recipe", "bottles", "ingredient" and "equipment"

2 join tables were created "equipment_recipe" and "ingredient_recipe"

there is a one to many relationship between "bottles" and "recipe"
    I added bottles_id in recipe
    and recipe_id in bottle.

    the requests were created to see the file requete.md




Base de donnée créé avec phpmyadmin

C'est une base de donnée qui regroupe des recettes cosmétiques.
Le nom de la base de donnée "cosmetic_recipe"

le script pour créer cette base de donnée est dans le dossier sql, il est composé de trois fichiers :
    "cosmetic_recipe.sql" fichier qui correspond à la base de donnée.
    "cosmetic_recipe_données.sql" qui correspond aux donnée.
    "cosmetic_recipe_structure.sql" qui correspond à la structure.

Un dossier "procedure" qui contient deux fichiers un pour la sauvegarde et l'autre pour restaurer la base de donnée.

Elle comprend 4 tables "recipe", "bottles", "ingredient" et "equipment"

2 tables de jointure ont été créé "equipment_recipe" et "ingredient_recipe"

il y un relation one to many entre "bottles" et "recipe"
    j'ai rajouté bottles_id dans recette
    et recipe_id dans flaconnage.

    les requettes ont eté crée voir le fichier requete.md



