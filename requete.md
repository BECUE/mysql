Request to recover ingredients from recipes
SELECT * 
FROM `recipe` 
INNER JOIN ingredient_recipe ON recipe.id = ingredient_recipe.recipe_id
INNER JOIN ingredient ON ingredient_recipe.ingredient_id = ingredient.id

request to recover recipes from the ingredients
SELECT * 
FROM `ingredient` 
INNER JOIN ingredient_recipe ON ingredient.id = ingredient_recipe.ingredient_id
INNER JOIN recipe ON ingredient_recipe.recipe_id = recipe.id

request to recover recipes from the materials
SELECT *
FROM `equipment`
INNER JOIN equipment_recipe ON equipment.id = equipment_recipe.equipment_id
INNER JOIN recipe ON equipment_recipe.recipe_id = recipe.id

request to recover materials from recipes
SELECT *
FROM `recipe`
INNER JOIN equipment_recipe ON recipe.id = equipment_recipe.recipe_id
INNER JOIN equipment ON equipment_recipe.equipment_id = equipment.id

request relation one to many "bottles" and "recipe"
SELECT * 
FROM `bottles` 
INNER JOIN recipe ON bottles.id = recipe.bottles_id

request relation many to one "recipe" and "bottles"
SELECT * 
FROM `recipe` 
INNER JOIN bottles ON recipe.id = bottles.recipe_id


