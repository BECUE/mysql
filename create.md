-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Mar 22 Janvier 2019 à 10:00
-- Version du serveur :  10.1.37-MariaDB-0+deb9u1
-- Version de PHP :  7.0.33-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- data base :  `cosmetic_recipe`
--

-- --------------------------------------------------------

--
--  table structure `bottles`
--

CREATE TABLE `bottles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
--  table structure `equipment`
--

CREATE TABLE `equipment` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
--  table structure `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `measured` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
--  table structure`recipe`
--

CREATE TABLE `recipe` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operating_mode` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precaution` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index for exported tables
--

--
-- Index for the table `bottles`
--
ALTER TABLE `bottles`
  ADD PRIMARY KEY (`id`);

--
-- Index for the table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Index for the table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Index for the table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bottles`
--
ALTER TABLE `bottles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;




modifying my tables

-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Mar 22 Janvier 2019 à 11:04
-- Version du serveur :  10.1.37-MariaDB-0+deb9u1
-- Version de PHP :  7.0.33-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- data base :  `cosmetic_recipe`
--

-- --------------------------------------------------------

--
--  table  `bottles`
--

CREATE TABLE `bottles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
--  table structure `equipment`
--

CREATE TABLE `equipment` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
--  table structure `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
--  table structure  `recipe`
--

CREATE TABLE `recipe` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operating_mode` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precaution` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contents of the table `recipe`
--

INSERT INTO `recipe` (`id`, `name`, `operating_mode`, `precaution`) VALUES
(1, 'savon', 'La méthode utilisée est la saponification.\r\n\r\nIl faut 500g d\'huile d\'olive, 170g d\'eau et 65g de cristaux de soude.\r\n\r\n1- dissoudre les cristaux dans l\'eau. La préparation va monter à 90°c donc il faut laisser refroidir 20min.\r\n\r\n2- Une fois refroidit, verser l\'huile.\r\n\r\n3- mixer jusqu\'à ce que le tout soit bien homogène. \r\n\r\n4- on peut ajouter des quelques gouttes d\'huiles essentielles. Si on rajoute de l\'huile essentielle, il faut remixer environ 1min.\r\n\r\n5- mettre dans des moules.\r\n\r\n6- laisser 1 semaine dans les moules.\r\n\r\n7- de 4 à 5 semaines avant utilisation dans un lieu sec. \r\n', 'Faire attention la réaction chimique des cristaux de soude avec l\'eau reste toxique, ne pas respirer.\r\n\r\nSe converse au sec.'),
(2, 'déodorant', 'Il faut 100g de d\'huile de coco, 60g de pierre d\'alun en poudre, 40g d\'arrow-root et 30 gouttes de goscard(conservateur)\r\n\r\nFaire fondre au bain marie l\'huile de coco puis ajouter la poudre d\'alun et le arrow-root.\r\n\r\nLa préparation doit être homogène.\r\n  \r\nMettre 30 gouttes de goscard.\r\n\r\nVersez la préparation dans un petit bocal en verre.\r\n\r\nLaissez refroidir au frigo 30 min.\r\n\r\nOn peut l\'utiliser.\r\n', 'Se conserve maximum 6 mois'),
(3, 'shampoing solide', 'Il faut 15g d\'huile de coco, 10g de shikakai, 50g de sodium de cocoyl isothionate, 5g glycérine végétale et 10g d\'eau.\r\n\r\n1- Au bain marie mélanger la glycérine et l\'huile.\r\n\r\n2- ajouter le shikakai et bien mélangé.\r\n\r\n3- ajouter le sodium.\r\n\r\n4- une fois bien mélangé, on ajoute l\'eau.\r\n\r\n5 - mettre dans un moule\r\n\r\n6- 10 min au frigo.\r\n\r\nOn peut utiliser le shampoing\r\n\r\n', ''),
(4, 'crème pour le visage', 'Il faut 60 ml de gel d’aloès vera, 28g d\'huile de coco, 8g de cire émulsifiante et 19g de goscard(conservateur).\r\n\r\nOn peut remplacer le gel d’aloès vera par de l\'eau.\r\n\r\nL\'huile de coco peut être remplacé par du beurre de karité, avocat ou mangue.\r\n\r\n1- mettre tous les ingrédients dans un bol puis  au bain marie sauf le conservateur.\r\n\r\n2- remuer lentement jusqu\'à ce que la cire fonde.\r\n\r\n3- retirer du bain marie, bien mélanger pour faire prendre l\'émulsion.\r\n\r\n4- Placer le bol dans l\'eau froide pour que la crème prenne et s\"épaississe puis rapidement.\r\n\r\n5- mettre le goscard.\r\n\r\nOn peut ajouter quelques gouttes d\'huiles essentielles ou de flagrance.\r\n\r\n\r\n', 'A utiliser dans les 6 mois.');

--
-- Index pour les tables exportées
--

--
-- Index for the table`bottles`
--
ALTER TABLE `bottles`
  ADD PRIMARY KEY (`id`);

--
-- Index for the table`equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Index for the table`ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Index for the table`recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bottles`
--
ALTER TABLE `bottles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;